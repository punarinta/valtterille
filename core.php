<?php

/**
 * Class Processor
 */
class Processor
{
    /**
     * This one is called by uploader and dispatches its data further
     *
     * @return int
     * @throws Exception
     */
    public function run()
    {
        // dispatch operation mode
        switch ((int) $_GET['mode'])
        {
            case 1:
                return $this->mode_1();

            case 2:
                return $this->mode_2();

            case 3:
                return $this->mode_3();

            default:
                throw new Exception('Hacking attempt');
        }
    }

    /**
     * Safely return POSTed data
     *
     * @param $key
     * @return null
     */
    private function getPosted($key)
    {
        return isset ($_POST[$key]) ? $_POST[$key] : null;
    }

    /**
     * This can execute your program with various bells-and-whistles
     *
     * @param $cmd
     * @return string
     */
    protected function execLog($cmd)
    {
        $log = '';

        $descriptors = array
        (
            0 => ['pipe', 'r'],
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w'],
        );
        $env = array
        (
            'PATH'      => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
            'LC_ALL'    => 'en_US.UTF-8',
            'LANG'      => 'en_US.UTF-8',
        );

        flush();
        $process = proc_open($cmd, $descriptors, $pipes, realpath('./'), $env);

        if (is_resource($process))
        {
            while ($s = fgets($pipes[1]))
            {
                $log .= $s;
                flush();
            }
        }

        proc_close($process);

        return $log;
    }

    /**
     * Upload mode #1
     *
     * @return string
     * @throws Exception
     */
    private function mode_1()
    {
        // check files
        if (!isset ($_FILES['textFile']))
        {
            throw new Exception('No text file provided');
        }

        $textFileName = $_FILES['textFile']['tmp_name'];

        return $this->execLog('./checktimestamp ' . $textFileName);
    }

    /**
     * Upload mode #2
     *
     * @return string
     * @throws Exception
     */
    private function mode_2()
    {
        // check files
        if (!isset ($_FILES['textFile']))
        {
            throw new Exception('No file provided');
        }

        if (!isset ($_FILES['file']))
        {
            throw new Exception('No file provided');
        }

        $fileName = $_FILES['file']['tmp_name'];
        $textFileName = $_FILES['textFile']['tmp_name'];

        // obfuscate file name to prevent other users accessing files not belonging to them
        $newFileName = 'storage/' . md5('3981' . microtime(true)) . '.tmp';

        // copy the second file
        copy($fileName, $newFileName);

        $this->execLog('./freebie ' . $textFileName . ' ' . $newFileName);

        // return a link to that file, a relative HTTP link equals to relative local file path in our case
        echo '<a href=' . $newFileName . '>Download the file</a>';
    }

    /**
     * Upload mode #3
     *
     * @return string
     * @throws Exception
     */
    private function mode_3()
    {
        // check files
        if (!isset ($_FILES['textFile']))
        {
            throw new Exception('No text file provided');
        }

        $textFileName = $_FILES['textFile']['tmp_name'];


        // check email
        if (!($email = $this->getPosted('email')))
        {
            throw new Exception('No email provided');
        }

        // check text
        if (!($text = $this->getPosted('textField')))
        {
            throw new Exception('No text provided');
        }

        // allow alphanumeric only
        $text = preg_replace('/[^a-zA-Z0-9]/', '', $text);

        // validate email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            throw new Exception('Malformed email address');
        }

        return $this->execLog('./requestkey ' . $textFileName . ' ' . $text . ' ' . trim($email));
    }
}
