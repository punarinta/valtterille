<?php

// this file is launched by cron and destroys all the files in 'public/storage' that are older than 5 minutes

// the only place we need, so go there
chdir(__DIR__ . '/public/storage');

foreach (glob('*.tmp') as $file)
{
    if (filemtime($file) < time() - 5 * 60)
    {
        // kill it with fire
        unlink($file);
    }
}
