<?php

// set CWD to the file's one and load core
chdir(__DIR__);
require_once '../core.php';

// start processor in a safe place, outside publicly accessible directory
try
{
    echo (new Processor)->run();
}
catch (Exception $e)
{
    // I guess your program should not be much verbose, so adjust verbosity yourself
    echo 'Error: ' . $e->getMessage();
}
